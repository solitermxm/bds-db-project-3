# Semester project 3 

## Hogwarts Data Base

The main task of our project was to create a graphical interface for SQL databases, which we did in previous projects for the academic semester.
The main parts of the project are the creation of tables in the database, CRUD operations, user authentication from the database, SQL injection, database backup




## How to run
need to run our database server with
```sh
docker-compose up
```
 we need to download required packages for python using this command:


```sh
python -m pip install -r requirements.txt
```

We will go to src/ directory and run the following command:
```sh
python manage.py runserver
```

## Used libraries

| libraries | versions |
| ------ | ------ |
| Django	 | 4.0	|
| bcrypt	 |3.2.0	|
| psycopg2| 2.9.2	 |
| asgiref| 3.4.1	 |
| sqlparse	| 0.4.2  |
